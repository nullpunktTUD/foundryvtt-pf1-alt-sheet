# 0.3.2

- Fix some layouting with CN locale
- Update AIP API to v2
- Implement item list filtering via search box like default sheet

# v0.3.1

- Use the conditions display/toggle-thingy from the default sheet

# v0.3.0

- Compat with Foundry v0.8.6 and PF1 >v0.78.0
- Moved spellslot progression radio buttons up

# v0.2.4

- Added a tooltip to the caster level in spellbooks to show the roll + bonus
- Autoconvert the speed to metric. This forces you to enter the speed in ft even when using metric.
  - There is a tooltip explaining that you need to do this, but there is no automatic migration.
  - This is done to do the same as the default sheet because the converted value is now provided to Drag Ruler.
- Added a checkbox to the class list to switch the layout to something more compact.
- Use a different CSS class for the instructions in the features tab to improve compatibility with Koboldworks
- Fix charges not being shown on the summary tab.
- Fix compatibility with PF1 0.77.22, especially the new spell slot system.
  - Warning: Some things might be missing/not working yet!
